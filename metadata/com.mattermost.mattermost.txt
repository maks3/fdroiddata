Categories:Internet
License:Apache-2.0
Web Site:https://about.mattermost.com/
Source Code:https://github.com/mattermost/mattermost-android-classic
Issue Tracker:https://github.com/mattermost/mattermost-android-classic/issues

Auto Name:Mattermost Classic
Summary:Secure, workplace messaging
Description:
Mattermost is secure, workplace messaging from behind your firewall.

- Discuss topics in private groups, one-to-one or team-wide - Find what you're
looking for with full text search, filters and #hashtags - Easily share images,
video, audio and files - Personalize your experience with custom colors,
formatting and fonts - Connect in-house systems with webhooks and
Slack-compatible integrations

To use this app, you need a "Team URL" from a Mattermost server. To host your
own server go to http://www.mattermost.org/download/

Find the Terms of Service for this app here: https://about.mattermost.com/terms

This is a reference implementation of an open source Mattermost Android app
available at: https://github.com/mattermost/mattermost-android-classic
.

Repo Type:git
Repo:https://github.com/mattermost/mattermost-android-classic.git

Build:3.10.2,398
    commit=v3.10.2
    subdir=app
    patch=play-services.patch
    gradle=yes
    prebuild=rm -r src/main/java/com/mattermost/gcm/

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:3.10.2
Current Version Code:398
